﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Sherlock;

public struct ModsStats
{
    public string FileName { get; set; }

    public int Count { get; set; }

    public List<string> Authors { get; set; }
}

public static class Program
{
    public static void Main(string[] args)
    {
        if (File.Exists("db.db")) File.Delete("db.db");
        if (File.Exists("db-log.db")) File.Delete("db-log.db");

        var db = new LiteDB.LiteDatabase("db.db");

        var sw = new Stopwatch();

        sw.Start();
        new StatsProcessor().Run(args[0], db);
        sw.Stop();
        Console.WriteLine($"Parsed Git output in {sw.ElapsedMilliseconds}ms");

        var stats = db.GetCollection<FileAuthorStat>();
        Console.WriteLine($"Processed {stats.Count()} file changes");

        sw.Restart();
        ExportJson(stats, "out.json");
        sw.Stop();
        Console.WriteLine($"Exported base info in {sw.ElapsedMilliseconds}ms");

        sw.Restart();
        PrintTop(stats, 10);
        sw.Stop();
        Console.WriteLine($"Printed top in {sw.ElapsedMilliseconds}ms");
    }

    private static void ExportJson(LiteDB.ILiteCollection<FileAuthorStat> stats, string jsonPath)
    {
        using var stream = File.OpenWrite(jsonPath);
        using var writer = new System.Text.Json.Utf8JsonWriter(stream);
        writer.WriteStartArray();
        foreach (var stat in stats.FindAll())
        {
            writer.WriteStartObject();
            writer.WriteString("author", stat.Author);
            writer.WriteString("date", stat.Date);
            writer.WriteString("title", stat.Title);
            writer.WriteString("mod", stat.Modification);
            writer.WriteString("fileName", stat.FileName);
            writer.WriteEndObject();
        }
        writer.WriteEndArray();
    }

    private static void PrintTop(LiteDB.ILiteCollection<FileAuthorStat> stats, int entriesCount = 5, int authorsCount = 3)
    {
        var comps = new Dictionary<string, ModsStats>();

        foreach (var stat in stats.FindAll())
        {
            if (stat.Modification != "M")
                continue; //We ignore any non-modification for now

            if (!comps.ContainsKey(stat.FileName))
                comps[stat.FileName] = new ModsStats
                {
                    FileName = stat.FileName,
                    Count = 1,
                    Authors = new List<string> { stat.Author }
                };
            else
            {
                var mstat = comps[stat.FileName];

                mstat.Count++;
                mstat.Authors.Add(stat.Author);

                comps[stat.FileName] = mstat;
            }
        }

        var ordered = comps.Values.OrderByDescending(x => x.Count).Take(entriesCount);

        Console.WriteLine($"\nTop {entriesCount} files modified, showing top {authorsCount} authors each:\n");

        foreach (var stat in ordered)
        {
            Console.WriteLine($"File: {stat.FileName} - modified {stat.Count} times");

            var authors = stat.Authors
                .GroupBy(s => s)
                .Select(g => (Author: g.Key, Count: g.Count()))
                .OrderByDescending(g => g.Count)
                .Take(authorsCount);

            foreach (var (author, count) in authors)
                Console.WriteLine($"-> {author}: {count} times");

            Console.WriteLine();
        }
    }
}