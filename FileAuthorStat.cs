﻿using System;

namespace Sherlock;

public struct FileAuthorStat
{
    public string Author { get; set; }

    public DateTime Date { get; set; }

    public string Title { get; set; }

    public string Modification { get; set; }

    public string FileName { get; set; }
}