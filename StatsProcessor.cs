﻿using LiteDB;
using System;
using System.Diagnostics;

namespace Sherlock;

public class StatsProcessor
{
    private string author = "";
    private string date = "";
    private string title = "";

    private void AddStat(ILiteCollection<FileAuthorStat> stats, string mod, string fileName)
    {
        stats.Insert(new FileAuthorStat
        {
            Author = author,
            Date = DateTime.Parse(date),
            Title = title,
            Modification = mod,
            FileName = fileName
        });
    }

    public void Run(string path, LiteDatabase db)
    {
        var process = CreateGitProcess(path);

        process.Start();

        var stats = db.GetCollection<FileAuthorStat>();

        using var reader = process.StandardOutput;

        while (!reader.EndOfStream)
        {
            var line = reader.ReadLine();

            if (line == null) break;

            line = line.Trim();

            if (line.EndsWith(">"))
                (author, date, title) = ParseAuthorLine(line);
            else if (line == string.Empty)
                continue;
            else
            {
                var (mod, fileName) = ParseFileLine(line);

                AddStat(stats, mod, fileName);
            }
        }
    }

    private static Process CreateGitProcess(string path)
    {
        var proc = Process.Start(new ProcessStartInfo
        {
            WorkingDirectory = path,
            FileName = "git",
            Arguments = "log --pretty=\"format:%ae > %aI > %f >\" --name-status",
            RedirectStandardOutput = true,
        });

        return proc;
    }

    private static (string mod, string fileName) ParseFileLine(string line)
    {
        var parts = line.Split('\t', StringSplitOptions.TrimEntries);

        return (parts[0], parts[1]);
    }

    private static (string author, string date, string title) ParseAuthorLine(string line)
    {
        var parts = line.Split(">", StringSplitOptions.TrimEntries);

        return (parts[0], parts[1], parts[2]);
    }
}